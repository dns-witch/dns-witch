# DNS Witch

A DNS zone management service to make accessible the [.eu.org](https://nic.eu.org) domain names to everyone.

